# 263650d-mavros

*Bug description here*

## What happens

The bug manifests as a node exiting with an error, as no messages have been
received on the two topics it subscribes to for ten seconds.

## How to reproduce

The example is run from within a docker container, using `docker-compose`. The
service is called *ros*.

To install `docker-compose`, follow the
[instructions](https://docs.docker.com/compose/install). For more
information about `docker-compose` itself, the
[documentation](https://docs.docker.com/compose/).

The main commands are:

- ```docker-compose up ros```: runs the example and exits.
- ```docker-compose run ros```: runs the example and opens an interactive
    shell in the container.
- ```docker-compose run ros bash```: creates the container and opens an
    interactive shell in it, but doesn't run the bug.

Being this just a simple `docker-compose` setup, any other standard
commands are also available 😏.


## Filesystem structure

The `mwe` directory is the root of the ROS package source code. The source
files are in `mwe/src`:


- `mavcmd`: Python file that starts a ROS node and subscribes to two topics.
    If no message is received after ten seconds, it exits with an error.
- `global_position.cpp`: C++ node that publishes to two topics every 100 ms.
- `launch.launch`: ROS launch file that starts the `global_position` node.
    It is not strictly necessary, but it is extremely convenient.
