#!/usr/bin/env bash

BASE_DIR=$(dirname "$0")

source "$BASE_DIR/catkin_ws/devel/setup.bash"

roslaunch "$BASE_DIR/catkin_ws/src/mwe/src/launch.launch" &
sleep 5s
echo ros master launched
echo starting mavcmd
rosrun mwe mavcmd

exec "$@"
