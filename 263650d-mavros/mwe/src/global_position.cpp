#include "ros/ros.h"
#include "std_msgs/String.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "global_position");

    ros::NodeHandle nh("~global_position");

    ros::Publisher raw_fix_pub = nh.advertise<std_msgs::String>("raw/fix", 10);
    ros::Publisher global_pub = nh.advertise<std_msgs::String>("global", 10);

    std_msgs::String msg;
    msg.data = "msg";

    ros::Rate loop_rate(10);

    while (ros::ok()) {
        raw_fix_pub.publish(msg);
        global_pub.publish(msg);

        loop_rate.sleep();
        ros::spinOnce();
    }

    return 0;
}
